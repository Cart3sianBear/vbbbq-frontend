import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import './plugins/vuetify'
import VueCookies from 'vue-cookies';
import VSwitch from 'v-switch-case';
import VueDraggableResizable from 'vue-draggable-resizable'
import VueWindowSize from 'vue-window-size';

import App from './App.vue'
import store from './store'
import Error404 from './components/Error404'
import MainContent from './components/MainContent'
import ConfirmEmail from './components/emailed/ConfirmEmail'
import RecoverEmail from './components/emailed/RecoverEmail'
import ForgottenPassword from './components/emailed/ForgottenPassword'
import ConfirmDeletion from './components/emailed/ConfirmDeletion'

import User from './classes/User'
import Conversation from './classes/Conversation'
import Entry from './classes/Entry'
import Geocoder from './classes/Geocoder'

import Conversations from './plugins/conversations'
import Entries from './plugins/entries'

Vue.config.productionTip = false

// router

const routes = [
  { path: '/email/confirm', component: ConfirmEmail },
  { path: '/email/recover', component: RecoverEmail },
  { path: '/password/new', component: ForgottenPassword },
  { path: '/deletion/confirm', component: ConfirmDeletion },
  { path: '/', component: MainContent },
  { path: '*', component: Error404 }
];
const router = new VueRouter({ routes, mode: 'history' });

// socket.io

const socketIOClient = require('socket.io-client');
const sailsIOClient = require('sails.io.js');

Vue.prototype.$io = sailsIOClient(socketIOClient)
Vue.prototype.$io.sails.url = process.env.VUE_APP_URL
Vuex.Store.prototype.$io = Vue.prototype.$io

// Global objects
Vue.prototype.$window = window;
Vue.prototype.$screen = screen;
Vue.prototype.$navigator = navigator;

// build & render

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueCookies);
Vue.use(VSwitch);

Vue.use(Conversations,{ io: Vue.prototype.$io, store: store});
Vue.use(Entries,{ io: Vue.prototype.$io, store: store});

import 'vue-draggable-resizable/dist/VueDraggableResizable.css'
Vue.component('vue-draggable-resizable', VueDraggableResizable)
Vue.use(VueWindowSize);

new Vue({
  router,
  store,
  User,
  Conversation,
  Entry,
  Geocoder,
  Conversations,
  Entries,

  render: h => h(App),
  created: function () {
    
    this.$io.socket.on('connect', () => {
      this.$store.dispatch('me/getMe')
      .then( () => { return this.$store.dispatch('accountPanelView/displayView', 'loggedIn', { root: true }); } );
    });

  }
}).$mount('#app')
