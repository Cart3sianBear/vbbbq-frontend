import User from '../../../classes/User'

export default {
  state: {
    users: [],
    loadingUsersId: [],
  },

  getters: {
    user: (state) => (id) => {
      return state.users.find(user => user.id === id);
    },
    users: (state, getters) => (ids) => {
      return ids.length==0 ? [] : ids.map( (element) => {
        return getters['user'](element);
      });
    },
  },

  actions: {

    loadUsers({ state, dispatch }, usersIdToLoad) {
      return new Promise( (resolve) => {
        usersIdToLoad = usersIdToLoad.filter( (element) => { return !state.users.map( (element2) => { return element2.id; } ).concat(state.loadingUsersId).includes(element); } );
        if (usersIdToLoad.length!=0) {
          usersIdToLoad.forEach( (element) => { state.loadingUsersId.push(element); });
          this.$io.socket.post('/api/get-users', { 'ids': usersIdToLoad, '_csrf': window.$cookies.get('_csrf') }, (res, jwr) => {
            usersIdToLoad.forEach( (element) => { state.loadingUsersId.splice(state.loadingUsersId.indexOf(element),1); });
            switch(jwr.statusCode) {
              case 200:
                usersIdToLoad.forEach( (userId) => {
                  if(!res.map( (element) => { return element.id } ).includes(userId)) {
                    res.push({
                      id: userId,
                      username: 'Deleted User',
                      city: ' ',
                      admin: false,
                      lastSeenAt: 0,
                      avatarsBuffered: { 32 : ' ', 40 : ' ', 64: ' ', 128: ' ', original : ' ', tmp : ' ' },
                    });
                  }
                });
                dispatch('storeUsers',res)
                .then(resolve);
              break;
            }
          });
        }
        else {
          resolve();
        }
      });
    },

    storeUsers({ state, commit }, usersToStore) {
      return new Promise( (resolve) => {
        usersToStore.forEach(element => {
          if(!state.users.find(user => user.id === element.id)) {
            commit('storeUser',element);
          }
        });
        resolve();
      });
    },

  },

  mutations: {
    storeUser(state, data) {
      state.users.push(new User(data,{ io: this.$io, store: this }));
    }
  },

  namespaced: true
} 
