import Conversation from '../../../classes/Conversation'

export default {
  state: {
    conversations: [],
  },

  getters: {
    conversation: (state) => (id) => {
      return state.conversations.find(conversation => conversation.id === id);
    },
    conversations: (state, getters) => (ids) => {
      return ids.length==0 ? [] : ids.map( (element) => {
        return getters['conversation'](element);
      });
    },
  },

  actions: {

    loadConversations({ dispatch }, conversationsIdToLoad) {
      return new Promise( (resolve) => {
        conversationsIdToLoad = conversationsIdToLoad.filter( (element) => { return !this.state.conversations.conversations.map( (element2) => { return element2.id; } ).includes(element); } );
        if (conversationsIdToLoad.length!=0) {
          this.$io.socket.post('/api/get-conversations', { 'ids': conversationsIdToLoad, '_csrf': window.$cookies.get('_csrf') }, (res, jwr) => {
            switch(jwr.statusCode) {
              case 200:
                dispatch('storeConversations',res.conversations)
                .then( () => { return dispatch('entries/storeEntries',res.entries,{ root: true }) });
                resolve();
              break;
            }
          });
        }
        else {
          resolve();
        }
      });
    },


    storeConversations({ state, commit }, conversationsToStore) {
      return new Promise( (resolve) => {
        var promiseUsersLoading = Promise.resolve();
        conversationsToStore.forEach(element => {
          promiseUsersLoading = promiseUsersLoading.then( () => {
            if(element.guestsId.includes(this.state.me.user.id) && !this.state.me.user.invitationsId.includes(element.id)) {
              this.state.me.user.invitationsId.push(element.id);
            }
            else if(element.participantsId.includes(this.state.me.user.id) && !this.state.me.user.conversationsId.includes(element.id)) {
              this.state.me.user.conversationsId.push(element.id);
            }
            else if(element.formerMembersId.includes(this.state.me.user.id) && !this.state.me.user.previousConversationsId.includes(element.id)) {
              this.state.me.user.previousConversationsId.push(element.id);
            }

            if(!state.conversations.find(conversation => conversation.id == element.id)) {
              commit('storeConversation',element);
              return state.conversations.find( (conversation) => { return conversation.id == element.id }).loadRelatedUsers();
            }
            else {
              return Promise.resolve();
            }
          });
        });
        promiseUsersLoading.then(resolve);
      });
    },

    editConversations({ state, commit }, conversationsToEdit) {
      return new Promise( (resolve) => {
        var promiseUsersLoading = Promise.resolve();
        conversationsToEdit.forEach(element => {
          promiseUsersLoading = promiseUsersLoading.then( () => {
            if(element.guestsId!=undefined && !element.guestsId.includes(this.state.me.user.id) && this.state.me.user.invitationsId.includes(element.id)) {
              this.state.me.user.invitationsId.splice(this.state.me.user.invitationsId.indexOf(element.id),1);
            }
            if(element.participantsId!=undefined && !element.participantsId.includes(this.state.me.user.id) && this.state.me.user.conversationsId.includes(element.id)) {
              this.state.me.user.conversationsId.splice(this.state.me.user.conversationsId.indexOf(element.id),1);
            }
            if(element.formerMembersId!=undefined && !element.formerMembersId.includes(this.state.me.user.id) && this.state.me.user.previousConversationsId.includes(element.id)) {
              this.state.me.user.previousConversationsId.splice(this.state.me.user.previousConversationsId.indexOf(element.id),1);
            }
            if(element.guestsId!=undefined && element.guestsId.includes(this.state.me.user.id) && !this.state.me.user.invitationsId.includes(element.id)) {
              this.state.me.user.invitationsId.push(element.id);
            }
            if(element.participantsId!=undefined && element.participantsId.includes(this.state.me.user.id) && !this.state.me.user.conversationsId.includes(element.id)) {
              this.state.me.user.conversationsId.push(element.id);
            }
            if(element.formerMembersId!=undefined && element.formerMembersId.includes(this.state.me.user.id) && !this.state.me.user.previousConversationsId.includes(element.id)) {
              this.state.me.user.previousConversationsId.push(element.id);
            }

            commit('replaceConversation',element);
            return state.conversations.find( (conversation) => { return conversation.id == element.id }).loadRelatedUsers();
          });
        });
        promiseUsersLoading.then(resolve);
      });
    },

    deleteConversations({ getters, commit }, ids) { // Not used atm
      return new Promise( (resolve) => {
        ids.forEach(id => {
          var element = getters['conversation'](id);
          if(this.state.me.user.invitationsId.includes(element.id)) {
            commit('wrappings/removeWrapping',id,{ root: true });
            element.entries.forEach( (entry) => {
              commit('entries/removeEntry',entry.id,{ root: true });
            });
            commit('removeConversation',id);
          }

          resolve();
        });
      });
    },



    createConversation({ dispatch }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/create-conversation', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('storeConversations', [res])
              .then( () => { return dispatch('mainContentView/displayView', 'dashboard', { root: true }); })
              .then(resolve(res.id));
            break;
          }
        });
      });
    },

    updateConversation({ getters, dispatch }, data) {
      return new Promise( (resolve) => {
        var conversation = getters['conversation'](data.id);
        if((data.lastCheckedAt!=null && conversation.unchecked) || (data.lastClickedAt!=null && conversation.unclicked) || (data.lastReadAt!=null && conversation.unread)) {
          data._csrf = window.$cookies.get('_csrf');
          this.$io.socket.post('/api/update-conversation', data, (res, jwr) => {
            switch(jwr.statusCode) {
              case 200:
                dispatch('editConversations', [res])
                .then(resolve);
              break;
            }
          });
        }
        else {
          resolve();
        }
      });
    },

    inviteInConversation({ dispatch }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/invite-in-conversation', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('editConversations', [res.conversation])
              .then( () => { return dispatch('entries/storeEntries', [res.entry], { root: true }); })
              .then(resolve);
            break;
          }
        });
      });
    },

    joinConversation({ dispatch }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/join-conversation', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('editConversations', [res.conversation])
              .then( () => { return dispatch('entries/storeEntries', [res.entry], { root: true }); })
              .then(resolve);
            break;
          }
        });
      });
    },

    leaveConversation({ dispatch }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/leave-conversation', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              var promise = Promise.resolve();
              promise = promise.then( () => { return dispatch('entries/storeEntries', res.entry!=null ? [res.entry] : [], { root: true }); })
              .then( () => { return dispatch('editConversations', [res.conversation]); })
              .then( () => { return dispatch('deleteConversations', [data.id]); });
              promise = promise.then( () => { return dispatch('wrappings/saveDashboard', '', { root: true }); })
              promise.then(resolve);
            break;
          }
        });
      });
    },

  },

  mutations: {
    storeConversation(state, data) {
      state.conversations.push(new Conversation(data,{ io: this.$io, store: this }));
    },

    replaceConversation(state, data) {
      delete data.entriesId;
      var conversation = state.conversations[state.conversations.indexOf(state.conversations.find( (element) => { return element.id == data.id; } ))];
      Object.keys(data).forEach( (key) => {
        conversation[key] = data[key];
      });
      state.conversations.splice(state.conversations.indexOf(state.conversations.find( (element) => { return element.id == data.id; } )),1,conversation);
    },

    removeConversation(state, id) {
      state.conversations.splice(state.conversations.indexOf(state.conversations.find( (element) => { return element.id == id; } )),1);
    },

    eraseConversations(state) {
      state.conversations = [];
    }
  },

  namespaced: true
}
