export default {
  state: {
    wrappings : [],
    fullScreenConversation : null
  },

  getters: {
    wrappedConversations: (state, getters, rootState, rootGetters) => () => {
      return state.wrappings.map( (wrapping) => {
        var wrappedConversation = { };
        Object.keys(wrapping).forEach( (key) => {
          wrappedConversation[key] = wrapping[key];
        });
        wrappedConversation.conversation = rootGetters['conversations/conversation'](wrapping.id);
        wrappedConversation.fullScreen = state.fullScreenConversation==wrappedConversation.id
        return wrappedConversation;
      } );
    }
  },

  actions: {

    storeWrappings({ rootGetters, dispatch, commit }, wrappingsToStore) {
      return new Promise( (resolve) => {
        var promiseConversationsLoading = Promise.resolve();
        wrappingsToStore.forEach(element => {
          promiseConversationsLoading = promiseConversationsLoading.then( () => {
            var conversation = rootGetters['conversations/conversation'](element.id);
            if(conversation==undefined) {
              return dispatch('loadConversations',[element.id])
              .then( () => {
                return new Promise( (resolve2) => {
                  commit('storeWrapping',element);
                  resolve2();
                });
              });
            }
            else {
              commit('storeWrapping',element);
              return Promise.resolve();
            }
          });
        });
        promiseConversationsLoading.then(resolve);
      });
    },

    editWrappings({ commit }, wrappingsToEdit) {
      return new Promise( (resolve) => {
        wrappingsToEdit.forEach(element => {
          commit('replaceWrapping',element);
        });
        resolve();
      });
    },

    saveDashboard({ state, rootState }) {
      return new Promise( (resolve) => {
        var ids = rootState.me.user.conversations.concat(rootState.me.user.invitations).map( (element) => { return element.id; } );
        this.$io.socket.post('/api/update-profile', { _csrf: window.$cookies.get('_csrf'), dashboard: { wrappings: state.wrappings.filter( (wrapping) => { return ids.includes(wrapping.id) && wrapping.x!=null && wrapping.y!=null && wrapping.w!=null && wrapping.h!=null && wrapping.z!=null } ), fullScreenConversation: ids.includes(state.fullScreenConversation) ? state.fullScreenConversation : null } }, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              resolve();
            break;
          }
        });
      });
    },


    toTopOfPile({ state, getters, dispatch }, id) {
      return new Promise( (resolve) => {
        var z = state.wrappings.find( (wrapping) => { return wrapping.id==id }).z;
        getters['wrappedConversations']().forEach( (element) => {
          if(z>=2 && element.z>z) {
            dispatch('editWrappings', [{
              id: element.id,
              z: element.z - 1
            }]);
          }
        });
        dispatch('editWrappings', [{
          id: id,
          z: getters['wrappedConversations']().length+2
        }]);
        dispatch('conversations/updateConversation', { id: id, lastCheckedAt: Date.now(), lastClickedAt: Date.now() }, { root: true });
        resolve();
      });
    },

    extend({ commit }, id) {
      return new Promise( (resolve) => {
        commit('extend', id);
        resolve();
      });
    },

    window({ commit }) {
      return new Promise( (resolve) => {
        commit('window');
        resolve();
      });
    },

    close({ state, getters, dispatch, commit }, id) {
      return new Promise( (resolve) => {
        var z = state.wrappings.find( (wrapping) => { return wrapping.id==id }).z;
        getters['wrappedConversations']().forEach( (element) => {
          if(element.z>z) {
            dispatch('editWrappings', [{
              id: element.id,
              z: element.z - 1
            }]);
          }
        });
        dispatch('conversations/updateConversation', { id: id, lastCheckedAt: Date.now(), lastClickedAt: Date.now() }, { root: true });
        commit('removeWrapping', id);
        resolve();
      });
    },
  },

  mutations: {

    setDashboard(state, data) {
      state.wrappings = data.wrappings;
      state.fullScreenConversation = data.fullScreenConversation;
    },

    storeWrapping(state, data) {
      if(!state.wrappings.map( (element) => { return element.id; } ).includes(data.id)) {
        state.wrappings.push(data);
      }
    },

    replaceWrapping(state, data) {
      var wrapping = state.wrappings[state.wrappings.indexOf(state.wrappings.find( (element) => { return element.id == data.id; } ))];
      delete data.id;
      Object.keys(data).forEach( (key) => {
        wrapping[key] = data[key];
      });
      // state.wrappings.splice(state.wrappings.indexOf(state.wrappings.find( (element) => { return element.id == data.id; } )),1,wrapping); // Produces duplicates wrappings
    },

    removeWrapping(state, id) {
      if(state.fullScreenConversation==id) {
       state.fullScreenConversation = null;
      }
      state.wrappings.splice(state.wrappings.indexOf(state.wrappings.find( (element) => { return element.id == id; } )),1);
    },

    extend(state, id) {
      state.fullScreenConversation = id;
    },

    window(state) {
      state.fullScreenConversation = null;
    },

    eraseWrappings(state) {
      state.wrappings = [];
    }
  },

  namespaced: true
}
