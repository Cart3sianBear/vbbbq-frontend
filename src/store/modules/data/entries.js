import Entry from '../../../classes/Entry'

export default {
  state: {
    entries: [],
  },

  getters: {
    entry: (state) => (id) => {
      return state.entries.find(entry => entry.id === id);
    },
    entries: (state, getters) => (ids) => {
      return ids.length==0 ? [] : ids.map( (element) => {
        return getters['entry'](element);
      });
    },
  },

  actions: {

    loadEntries({ dispatch }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/get-entries', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('storeEntries',res)
              .then(resolve);
            break;
          }
        });
      });
    },

    storeEntries({ state, rootGetters, commit }, entriesToStore) {
      return new Promise( (resolve) => {
        entriesToStore.forEach(element => {
          var conversation = rootGetters['conversations/conversation'](element.conversationId);
          if(!conversation.entriesId.includes(element.id)) {
            conversation.entriesId.push(element.id);
          }
          if(!state.entries.find(entry => entry.id === element.id)) {
            commit('storeEntry',element);
          }
        });
        resolve();
      });
    },

    editEntries({ commit }, entriesToEdit) {
      return new Promise( (resolve) => {
        entriesToEdit.forEach(entry => {
          commit('replaceEntry',entry);
          resolve();
        });
      });
    },

    deleteEntries({ commit }, ids) {
      return new Promise( (resolve) => {
        ids.forEach(id => {
          commit('replaceEntry',{ id: id, data: { }, deleted: true });
          resolve();
        });
      });
    },


    createEntry({ dispatch }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/create-entry', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('storeEntries', [res])
              .then(resolve);
            break;
          }
        });
      });
    },

    updateEntry({ dispatch }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/update-entry', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('editEntries', [res])
              .then(resolve);
            break;
          }
        });
      });
    },

    deleteEntry({ dispatch }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/delete-entry', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('deleteEntries', [data.id])
              .then(resolve);
            break;
          }
        });
      });
    },

  },

  mutations: {
    storeEntry(state, data) {
      state.entries.push(new Entry(data,{ io: this.$io, store: this }));
    },

    replaceEntry(state, data) {
      var entry = state.entries[state.entries.indexOf(state.entries.find( (element) => { return element.id == data.id; } ))];
      if(entry!=undefined) {
        Object.keys(data).forEach( (key) => {
          entry[key] = data[key];
        });
        state.entries.splice(state.entries.indexOf(state.entries.find( (element) => { return element.id == data.id; } )),1,entry);
      }
    },

    removeEntry(state, id) {
      state.entries.splice(state.entries.indexOf(state.entries.find( (element) => { return element.id == id; } )),1);
    },

    eraseEntries(state) {
      state.entries = [];
    }
  },

  namespaced: true
}
