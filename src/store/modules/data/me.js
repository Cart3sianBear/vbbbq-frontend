import User from '../../../classes/User'

export default {
  state: {
    user: null,
  },

  getters: {
    loggedIn: (state) => {
      return state.user!=null;
    }
  },

  actions: {
    register({dispatch, commit}, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/signup', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('accountPanelView/displayView', 'logIn', { root: true })
              .then( new Promise( (resolve) => {
                commit('accountPanelView/displayState', { statut : 'success', message : 'Successfully registered. Please check your mails.' }, { root: true });
                resolve();
              }))
              .then(resolve);
            break;
            default :
              commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
          }
        });
      });
    },

    logIn({ dispatch, commit }, data) {
     return new Promise( (resolve) => {
        var xhr = new XMLHttpRequest();
        xhr.open("PUT", process.env.VUE_APP_URL+"/api/login", true);
        data._csrf = window.$cookies.get('_csrf');
        xhr.addEventListener("load", () => {
          switch(xhr.status) {
            case 200:
              this.$io.socket.put('/api/login', data, (res, jwr) => {
                switch(jwr.statusCode) {
                  case 200:
                    dispatch('getMe')
                    .then( () => { return dispatch('accountPanelView/displayView', 'loggedIn', { root: true }); })
                    .then(resolve);
                  break;
                  default :
                    commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
                }
              });
            break;
            case 403:
              commit('accountPanelView/displayState', { statut : 'error', message : 'Please check your mails to confirm your account or ' }, { root: true }); // Ugly trick in component
            break;
            default :
              commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
          }
        });
        xhr.send(JSON.stringify(data));
      });
    },

    forgottenPassword({ commit }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/forgotten-password', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              commit('accountPanelView/displayState', { statut : 'success', message : 'It might have worked. Please check your emails' }, { root: true }); 
              resolve();
            break;
            default :
              commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
          }
        });
      });
    },

    getMe({ state, dispatch, commit }) {
      return new Promise( (resolve) => {
        this.$io.socket.get('/api/get-me', (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              if(res.dashboard!=null) {
                commit('wrappings/setDashboard', res.dashboard, { root: true });
                delete res.dashboard;
              }
              commit('setUser', res);
              if (state.user!=null) {
                state.user.loadRelatedConversations();
              }
              dispatch('mainContentView/displayView', 'dashboard', { root: true })
              .then(resolve);
            break;
          }
          this.state.getMeHandshaked = true;
        });
      });
    },

    updateProfile({ dispatch, commit }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/update-profile', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('accountPanelView/displayView', 'loggedIn', { root: true })
              .then( () => {
                commit('accountPanelView/displayState', { statut : 'success', message : 'Profile successfully updated' }, { root: true }); 
                return dispatch('getMe');
              })              
              .then(resolve);
            break;
            default :
              commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
          }
        });
      });
    },

    updateAvatar({ dispatch, commit }, data) {
      return new Promise( (resolve) => {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", process.env.VUE_APP_URL+"/api/update-avatar", true);
        xhr.setRequestHeader('X-CSRF-Token', window.$cookies.get('_csrf'));
        xhr.withCredentials = true;
        xhr.addEventListener("load", () => {
          switch(xhr.status) {
            case 200:
              commit('forgetAvatar','tmp');
              dispatch('accountPanelView/displayView', 'settings', { root: true })
              .then(resolve);
            break;
            default :
              commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
          }
        });
        var form = new FormData();
        form.append('avatar',data.avatar);
        xhr.send(form);
      });
    },

    cropAvatar({ commit }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/crop-avatar', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              commit('accountPanelView/displayState', { statut : 'success', message : 'Avatar successfully updated' }, { root: true }); 
              commit('forgetAllAvatars','tmp');
              resolve();
            break;
            default :
              commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
          }
        });
      });
    },

    logOut({ dispatch, commit }) {
      return new Promise( (resolve) => {
        this.$io.socket.get('/api/logout', (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('mainContentView/displayView', 'welcome', { root: true })
              .then( () => { return dispatch('wrappings/window', '', { root: true }); } )
              .then( () => { return dispatch('accountPanelView/displayView', 'logIn', { root: true }); } )
              .then( () => { return new Promise( (resolve2) => { commit('setUser', null); resolve2(); }); })
              .then( () => { return new Promise( (resolve2) => { commit('conversations/eraseConversations', null, { root: true }); resolve2(); }); })
              .then( () => { return new Promise( (resolve2) => { commit('entries/eraseEntries', null, { root: true }); resolve2(); }); })
              .then( () => { return new Promise( (resolve2) => { commit('wrappings/eraseWrappings', null, { root: true }); resolve2(); }); })
              .then( () => { return new Promise( (resolve2) => { commit('searchField/eraseSearches', null, { root: true }); resolve2(); }); })
              .then( () => { return new Promise( (resolve2) => { commit('recommendedConversations/eraseRecommendations', null, { root: true }); resolve2(); }); })
              .then(resolve);
            break;
            default :
              commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
          }
        });
      });
    },

    deleteAccount({ getters, dispatch, commit }, data) {
      return new Promise( (resolve) => {
        data._csrf = window.$cookies.get('_csrf');
        this.$io.socket.post('/api/delete-account', data, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              if(getters['loggedIn']) {
                dispatch('logOut')
                .then( () => {
                  commit('accountPanelView/displayState', { statut : 'success', message : 'Please check your emails... :(' }, { root: true }); 
                })
                .then(resolve);
              }
              else {
                resolve();
              }
            break;
            default :
              commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
          }
        });
      });
    },
  },

  mutations: {
    setUser(state, data) {
      if(data!=null) {
        state.user = new User(data,{ io: this.$io, store: this });
      }
      else {
        state.user = null; 
      }
    },

    forgetAvatar(state, size) {
      state.user.avatarsBuffered[size]=null;
    },

    forgetAllAvatars(state) {
      Object.keys(state.user.avatarsBuffered).forEach( (key) => {
        state.user.avatarsBuffered[key]=null;
      });
    }
  },

  namespaced: true
}
