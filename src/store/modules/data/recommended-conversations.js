export default {
  state: {
    recommendations : [],
  },

  getters: {
    recommendedConversations: (state, getters, rootState, rootGetters) => () => {
      return state.recommendations.map( (recommendation) => {
        var recommendedConversations = { };
        Object.keys(recommendation).forEach( (key) => {
          recommendedConversations[key] = recommendation[key];
        });
        recommendedConversations.conversation = rootGetters['conversations/conversation'](recommendation.id);
        return recommendedConversations;
      } );
    }
  },

  actions: {

    loadRecommendedConversations({ dispatch }) {
      return new Promise( (resolve) => {
        this.$io.socket.post('/api/get-recommended-conversations', { '_csrf': window.$cookies.get('_csrf') }, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('conversations/storeConversations',res.conversations,{ root: true })
              .then( () => { return dispatch('storeRecommendations',res.recommendations); });
              resolve();
            break;
          }
        });
      });
    },

    storeRecommendations({ commit }, recommendationsToStore) {
      return new Promise( (resolve) => {
        recommendationsToStore.forEach(element => {
          commit('storeRecommendation',element);
          resolve();
        });
      });
    },

    removeRecommendation({ commit }, id) {
      return new Promise( (resolve) => {
        commit('removeRecommendation', id);
        resolve();
      });
    },

  },

  mutations: {

    storeRecommendation(state, data) {
      if(!state.recommendations.map( (element) => { return element.id; } ).includes(data.id)) {
        state.recommendations.push(data);
      }
    },

    removeRecommendation(state, id) {
      state.recommendations.splice(state.recommendations.indexOf(state.recommendations.find( (element) => { return element.id == id; } )),1);
    },

    eraseRecommendations(state) {
      state.recommendations = [];
    }

  },

  namespaced: true
}
