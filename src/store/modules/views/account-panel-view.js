export default {
  state: {
    opened : false,
    displayedView : 'logIn', // ['logIn','registration','loggedIn','settings','changeEmail','changePassword','deleteAccount']
    displayedState : { statut : 'pending', message : '' }
  },

  getters: {

  },

  actions: {
    displayView({ commit }, name) {
      return new Promise( (resolve) => {
        commit('displayView', name);
        commit('displayState', { statut : 'pending', message : '' });
        // commit('close');
        // commit('open');
        resolve();
      });
    },
  },

  mutations: {
    open(state) {
      state.opened = true;
    },
    close(state) {
      state.opened = false;
    },
    displayView(state, name) {
      state.displayedView = name;
    },
    displayState(state, data) {
      state.displayedState = data;
    }
  },

  namespaced: true
}
