export default {
  state: {
    displayedView : 'welcome', // ['welcome','dashboard','meetingPlace']
  },

  getters: {

  },

  actions: {
    displayView({ commit }, name) {
      return new Promise( (resolve) => {
        commit('displayView', name);
        resolve();
      });
    },
  },

  mutations: {
    displayView(state, name) {
      state.displayedView = name;
    },
  },

  namespaced: true
}
