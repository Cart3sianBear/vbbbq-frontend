export default {
  state: {
    searchString: '',
    usersProposed: [],
    usersSelected: [],
    displayedState : { statut : 'pending', message : '' },

    draggedItem: null,
  },

  getters: {
    usersProposed: (state) => {
      // return rootGetters['users/users'](state.usersProposed).concat(state.usersSelected);
      return state.usersProposed.concat(state.usersSelected);
    }
  },

  actions: {
    search({ state, rootGetters, dispatch, commit }) {
      if(state.searchString!='') {
        this.$io.socket.post('/api/search', { 'text': state.searchString, '_csrf': window.$cookies.get('_csrf') }, (res, jwr) => {
          switch(jwr.statusCode) {
            case 200:
              dispatch('users/storeUsers',res.users,{ root: true });
              res.users.forEach( (element) => {
                rootGetters['users/user'](element.id).avatars.load('40');
              });
              commit('setSearchResponses', res.users);
            break;
            default :
              commit('displayError');
          }
        });
      }
      else {
        commit('clearSearchResponses');
      }
    },
    select({ state, rootGetters, commit }) {
      commit('clearSearchResponses');
      rootGetters['users/users'](state.usersSelected).forEach( (element) => {
        element.avatars.load('32');
      });
    },
  },

  mutations: {
    setSearchResponses(state, usersToPropose) {
      if(usersToPropose.length!=0) {
        state.usersProposed = usersToPropose.map( (element) => {
          return element.id;
        });
      }
      state.displayedState = { statut : 'pending', message : '' };
    },
    clearSearchResponses(state) {
      state.searchString = '';
      state.usersProposed = [];
      state.displayedState = { statut : 'pending', message : '' };
    },
    deselect(state, userToDeselect) {
      state.usersSelected.splice(state.usersSelected.indexOf(userToDeselect),1);
    },
    displayError(state) {
      state.displayedState = { statut : 'error', message : 'An error occured' };
      // state.displayedState = { statut : 'pending', message : 'No one fits this description... :/' };
    },
    eraseSearches(state) {
      state.searchString = '';
      state.usersProposed = [];
      state.usersSelected = [];
      state.displayedState = { statut : 'pending', message : '' };
    }
  },

  namespaced: true
}
