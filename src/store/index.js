import Vue from 'vue'
import Vuex from 'vuex'
import me from './modules/data/me'
import users from './modules/data/users'
import wrappings from './modules/data/wrappings'
import conversations from './modules/data/conversations'
import entries from './modules/data/entries'
import recommendedConversations from './modules/data/recommended-conversations'
import mainContentView from './modules/views/main-content-view'
import searchField from './modules/search-field'
import accountPanelView from './modules/views/account-panel-view'

// build

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    csrfReady: false,
    getMeHandshaked: false,
  },
  modules: {
    me,
    users,
    wrappings,
    conversations,
    entries,

    recommendedConversations,
    accountPanelView,
    mainContentView,

    searchField,
  }
})
