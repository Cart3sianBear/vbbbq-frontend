export default {
  install(Vue,injections) {

    Object.keys(injections).forEach( (key) => {
      this[key] = injections[key];
    });

    this.io.socket.on('entryCreated', (entry) => {
      var conversation = this.store.getters['conversations/conversation'](entry.conversationId);
      var previousState= { unread: conversation.unread, unclicked: conversation.unclicked, unchecked: conversation.unchecked }
      this.store.dispatch('entries/storeEntries', [entry])
      .then( () => {
        if(document.hidden && !previousState.unchecked && !previousState.unread) {
          var audio = new Audio(require('../assets/ding.flac'));
          audio.volume = 0.15;
          audio.play();
        }
      });
    });

    this.io.socket.on('entryEdited', (entry) => {
      this.store.dispatch('entries/editEntries', [entry]);
    });

  }
};
