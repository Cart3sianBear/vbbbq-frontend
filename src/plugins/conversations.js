export default {
  install(Vue,injections) {

    Object.keys(injections).forEach( (key) => {
      this[key] = injections[key];
    });

    this.io.socket.on('conversationCreated', (conversation) => {
      this.store.dispatch('conversations/storeConversations', [conversation])
      .then( () => {
        if(document.hidden) {
          var audio = new Audio(require('../assets/ding.flac'));
          audio.volume = 0.15;
          audio.play();
        }
      });
    });

    this.io.socket.on('conversationEdited', (conversation) => {
      if(!this.store.state.conversations.conversations.map( (element) => { return element.id; } ).includes(conversation.id)) {
        this.store.dispatch('conversations/storeConversations', [conversation]);
      }
      else {
        this.store.dispatch('conversations/editConversations', [conversation]);
      }
    });

    this.io.socket.on('conversationDestroyed', (id) => {
      this.store.dispatch('wrappings/close',{ id: id })
      .then( () => { return this.store.dispatch('conversations/deleteConversations', [id]); } );
    });

    const onUserChipDrop = (event) => {
      Vue.nextTick( () => {
        var element = document.elementFromPoint(event.detail.x,event.detail.y);
        while(!element.hasAttribute('id')) {
          element = element.parentElement;
        }
        if(element.id.split('Conversation')[1]!=undefined) {
          this.store.dispatch('conversations/inviteInConversation',{ conversationId: parseInt(element.id.split('Conversation')[1]), userId: event.detail.user.id });
        }
      });
    };
    document.addEventListener('userChipDrop', onUserChipDrop);

  }
};
