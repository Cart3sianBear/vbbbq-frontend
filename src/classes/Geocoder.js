export default function Geocoder() {
  this.osmQuery = function (text) {
    return new Promise( (resolve) => {
      var xhr = new XMLHttpRequest();
      xhr.open("GET", process.env.VUE_APP_NOMINATIM_URL+"/search.php?q="+text+"&format=json", true);
      xhr.addEventListener("load", () => {
        switch(xhr.status) {
          case 200:
            resolve(JSON.parse(xhr.response));
          break;
          // default :
          //   this.$store.commit('accountPanelView/displayState', { statut : 'error', message : 'An error occured...' }, { root: true });
        }
      });
      xhr.send();
    });
  }
}
