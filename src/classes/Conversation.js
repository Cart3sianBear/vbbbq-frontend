export default function Conversation(input,injections) {

  Object.keys(Object.assign(input,injections)).forEach( (key) => {
    this[key] = Object.assign(input,injections)[key];
  });

  ['participants','guests','formerMembers'].forEach( (designation) => {
    Object.defineProperty(this, designation, { get: () => {
      var ids = [...this[designation+'Id']];
      var users = [];
      if(ids.includes(this.store.state.me.user.id)) {
        ids.splice(ids.indexOf(this.store.state.me.user.id),1);
        users.push(this.store.state.me.user);
      }
      users = users.concat(this.store.getters['users/users'](ids));
      if (users!=null && !users.includes(undefined)) {
        return users;
      }
      else {
        return [];
      }
    }});
  });

  this.loadRelatedUsers = () => {
    var ids = this.participantsId.concat(this.guestsId);
    if(ids.includes(this.store.state.me.user.id)) {
      ids.splice(ids.indexOf(this.store.state.me.user.id),1);
    }
    return this.store.dispatch('users/loadUsers',ids); 
  };

  var promise = Promise.resolve();

  if(this.entries!=undefined) {
    promise.then( () => { return this.store.dispatch('entries/storeEntries', this.entries); } )
  }

  promise.then( () => {
    return new Promise( (resolve) => {
      Object.defineProperty(this, 'entries', { get: () => {
        var entries = this.store.getters['entries/entries'](this.entriesId);
        return entries!=null && !entries.includes(undefined)  ? entries : [];
      }});

      Object.defineProperty(this, 'lastMessage', { get: () => {
        return this.entries!=null && !this.entries.includes(undefined)  ? this.entries[this.entries.map( (entry) => { return entry.id; } ).indexOf(Math.max(...this.entries.filter( (entry) => { return entry.type=='create' || entry.type=='message' && !entry.deleted } ).map( (entry) => { return entry.id; } )))] : { createdAt: 0 };
      }});

      if(this.lastCheckedAt==null) { this.lastCheckedAt = 0; }
      if(this.lastClickedAt==null) { this.lastClickedAt = 0; }
      if(this.lastReadAt==null) { this.lastReadAt = 0; }

      Object.defineProperty(this, 'unchecked', { get: () => {
        return this.entries!=undefined && this.entries.some( (entry) => { return entry.createdAt>this.lastCheckedAt && ((entry.type=='invited' && entry.userId==this.store.state.me.user.id) || (entry.type=='message' && entry.userId!=this.store.state.me.user.id)); });
      }});

      Object.defineProperty(this, 'unclicked', { get: () => {
        return this.entries!=undefined && this.entries.some( (entry) => { return entry.createdAt>this.lastClickedAt && ((entry.type=='invited' && entry.userId==this.store.state.me.user.id) || (entry.type=='message' && entry.userId!=this.store.state.me.user.id)); });
      }});

      Object.defineProperty(this, 'unread', { get: () => {
        return this.entries!=undefined && this.entries.some( (entry) => { return entry.createdAt>this.lastReadAt && ((entry.type=='invited' && entry.userId==this.store.state.me.user.id) || (entry.type=='message' && entry.userId!=this.store.state.me.user.id)); });
      }});

      resolve();
    });
  });

  this.loadRelatedEntries = (until) => { 
    if(until==null) {
      return this.store.dispatch('entries/loadEntries',{ conversation: this.id }); 
    }
    else {
      return this.store.dispatch('entries/loadEntries',{ conversation: this.id, until: until }); 
    }
  };

}
