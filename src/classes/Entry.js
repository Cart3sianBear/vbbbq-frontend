export default function Entry(input,injections) {

  Object.keys(Object.assign(input,injections)).forEach( (key) => {
    this[key] = Object.assign(input,injections)[key];
  });

  Object.defineProperty(this, 'user', { get: () => {
    return this.userId==this.store.state.me.user.id ? this.store.state.me.user : this.store.getters['users/user'](this.userId);
  }});

  this.loadRelatedUser = () => {
    return this.store.dispatch('users/loadUsers',[this.userId]); 
  };

  Object.defineProperty(this, 'conversation', { get: () => {
    return this.store.getters['conversations/conversation'](this.conversationId);
  }});

}
