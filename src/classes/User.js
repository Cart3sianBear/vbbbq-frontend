export default function User(input, injections) {

  Object.keys(Object.assign(input,injections)).forEach( (key) => {
    this[key] = Object.assign(input,injections)[key];
  });

  this.avatars = { };
  Object.keys(input.avatarsBuffered).forEach( (key) => {
    Object.defineProperty(this.avatars, key, { get: () => {
      if([null,' '].includes(this.avatarsBuffered[key])) {
        return require('../assets/noavatar'+key+'.png');
      }
      else {
        return this.avatarsBuffered[key]
      }
    }});
  });

  this.avatarsLoadingFlags = { 32: false, 40: false, 64: false, original: false, tmp: false }
  this.avatars.load = (size) => {
    if(this.avatarsBuffered[size]==null && !this.avatarsLoadingFlags[size]) {
      this.avatarsLoadingFlags[size]=true;
      return Promise.resolve({ id: this.id, size: size }).then( ({ id, size }) => {
        return new Promise( (resolve) => {
          this.io.socket.get('/api/get-avatar?id='+id+'&size='+size, (res, jwr) => {
            switch(jwr.statusCode) {
              case 200:
                resolve(res);
              break;
              // default :
                // reject(new Error('An error occured...'));
            }
          });
        });
      }).then( (avatar) => {
        if(avatar!=null) {
          this.avatarsBuffered[size] = window.URL.createObjectURL(new Blob( [ avatar ], { type: "image/jpeg" } ));
        }
        else {
          this.avatarsBuffered[size] = require('../assets/noavatar'+size+'.png')
        }
        this.avatarsLoadingFlags[size]=false;
      });
    }
    else {
      return Promise.resolve(null);
    }
  };

  Object.defineProperty(this, 'conversations', { get: () => {
    var conversations = this.store.getters['conversations/conversations'](this.conversationsId);
    return conversations!=null && !conversations.includes(undefined) ? conversations : [];
  }});
  Object.defineProperty(this, 'invitations', { get: () => {
    var invitations = this.store.getters['conversations/conversations'](this.invitationsId);
    return invitations!=null && !invitations.includes(undefined) ? invitations : [];
  }});

  Object.defineProperty(this, 'previousConversations', { get: () => {
    var previousConversations = this.store.getters['conversations/conversations'](this.previousConversationsId);
    return previousConversations!=null && !previousConversations.includes(undefined) ? previousConversations : [];
  }});

  this.loadRelatedConversations = () => { return this.store.dispatch('conversations/loadConversations',this.conversationsId.concat(this.invitationsId)); };
  this.loadPreviousConversations = () => { return this.store.dispatch('conversations/loadConversations',this.previousConversationsId); };
}
