module.exports = {
  devServer: {
    https: process.env.FRONTEND_PROTOCOL=='https' ? true : false,
    port: process.env.FRONTEND_PORT,
    proxy: {
      '/socket.io': { 
        target: process.env.BACKEND_PROTOCOL+'://'+process.env.BACKEND_HOST+':'+process.env.BACKEND_PORT, 
        secure: process.env.FRONTEND_PROTOCOL=='https' ? true : false,
        // changeOrigin: true,
      },
      '/security': { 
        target: process.env.BACKEND_PROTOCOL+'://'+process.env.BACKEND_HOST+':'+process.env.BACKEND_PORT, 
        secure: process.env.FRONTEND_PROTOCOL=='https' ? true : false,
        // changeOrigin: true,
      },
      '/api': { 
        target: process.env.BACKEND_PROTOCOL+'://'+process.env.BACKEND_HOST+':'+process.env.BACKEND_PORT, 
        ws: true,
        secure: process.env.FRONTEND_PROTOCOL=='https' ? true : false,
        // changeOrigin: true,
      },
    }
  }
}
